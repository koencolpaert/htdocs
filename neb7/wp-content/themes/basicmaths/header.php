<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes() ?>>
<head profile="http://gmpg.org/xfn/11">
<title><?php bm_conditional_title(); ?></title>
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />

<?php wp_head(); ?>

<link rel="shortcut icon" href="<?php echo bloginfo('template_directory') ?>/img/favicon.ico" />
<link rel="apple-touch-icon" href="<?php echo bloginfo('template_directory') ?>/img/apple-touch-icon.png" />
<link rel="pingback" href="<?php bloginfo('pingback_url') ?>" />

<!--[if lt IE 8]>
	<script src="http://ie7-js.googlecode.com/svn/version/2.0(beta3)/IE8.js" type="text/javascript"></script>
<![endif]-->

</head>
 
<body <?php body_class(); ?>>

<div id="wrapper">

	<div id="header">
		<h1><a href="<?php echo get_option('home'); ?>/"><span><?php bloginfo('name'); ?></span></a></h1>
		<span class="description"><?php bloginfo('description'); ?></span>
					
	</div>
	
	<div id="skip">
		<a href="#content" title="<?php esc_attr_e( 'Skip navigation to the content', 'basicmaths' ); ?>"><span><?php _e( 'Skip to content', 'basicmaths' ); ?></span></a>
	</div>

	<div id="access">
	
		<?php bm_nav(); ?>

	</div>

