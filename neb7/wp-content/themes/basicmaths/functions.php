<?php

// Get Theme and Child Theme Data
// Credits: Joern Kretzschmar & Thematic http://themeshaper.com/thematic
$themeData = get_theme_data(TEMPLATEPATH . '/style.css');
$version = trim($themeData['Version']);
if(!$version)
    $version = "unknown";

$ct=get_theme_data(STYLESHEETPATH . '/style.css');
$templateversion = trim($ct['Version']);
if(!$templateversion)
    $templateversion = "unknown";

// Set theme constants
define('THEMENAME', $themeData['Title']);
define('THEMEAUTHOR', $themeData['Author']);
define('THEMEURI', $themeData['URI']);
define('BMVERSION', $version);

// Set child theme constants
define('TEMPLATENAME', $ct['Title']);
define('TEMPLATEAUTHOR', $ct['Author']);
define('TEMPLATEURI', $ct['URI']);
define('TEMPLATEVERSION', $templateversion);

// Path constants
define('TEMPLATEDIR', get_bloginfo('template_directory'));
define('STYLEURL', get_bloginfo('stylesheet_url'));

// 	Set Up Basic Maths
//	Tell WordPress to run bm_setup() when the 'after_setup_theme' hook is run.
add_action( 'after_setup_theme', 'bm_setup' );

if ( ! function_exists( 'bm_setup' ) ):
function bm_setup() {
	// This theme styles the visual editor with editor-style.css to match the theme style.
	add_editor_style();

	// This theme uses post thumbnails
	//	add_theme_support( 'post-thumbnails' );

	// Add default posts and comments RSS feed links to head
	add_theme_support( 'automatic-feed-links' );

	// Make theme available for translation
	// Translations can be filed in the /languages/ directory
	load_theme_textdomain( 'basicmaths', TEMPLATEPATH . '/languages' );

	//	Translations
	$locale = get_locale();
	$locale_file = TEMPLATEPATH . "/languages/$locale.php";
	if ( is_readable( $locale_file ) )
		require_once( $locale_file );

	// This theme uses wp_nav_menu() in one location.
	add_theme_support( 'menus' );
	register_nav_menus( array(
		'bm-menu' => __( 'Basic Maths Menu', 'basicmaths' ),
	) );

	// Add Basic Math Header
	define( 'HEADER_TEXTCOLOR', '' );
	// No CSS, just IMG call. The %s is a placeholder for the theme template directory URI.
	// define( 'HEADER_IMAGE', '%s/css/headers/path.jpg' );
	
	// The height and width of your custom header. You can hook into the theme's own filters to change these values.
	// Add a filter to twentyten_header_image_width and twentyten_header_image_height to change these values.
	define( 'HEADER_IMAGE_WIDTH', apply_filters( 'bm_header_image_width', 170 ) );
	define( 'HEADER_IMAGE_HEIGHT', apply_filters( 'bm_header_image_height', 150 ) );
	
	// We'll be using post thumbnails for custom header images on posts and pages.
	// We want them to be 170 pixels wide by 150 pixels tall.
	// Larger images will be auto-cropped to fit, smaller ones will be ignored. See header.php.
	set_post_thumbnail_size( HEADER_IMAGE_WIDTH, HEADER_IMAGE_HEIGHT, true );
	
	// Add Basic Maths Header styles for Admin
	add_custom_image_header( '', 'bm_admin_header_style' );

	// Default custom headers packaged with the theme. %s is a placeholder for the theme template directory URI.
	register_default_headers( array(
		'basicmaths' => array(
			'url' => '%s/img/logo.png',
			'thumbnail_url' => '%s/img/logo.png',
			'description' => __( 'Basic Maths Logo', 'basicmaths' )
		),
		'rainbow' => array(
			'url' => '%s/img/rainbow.png',
			'thumbnail_url' => '%s/img/rainbow.png',
			'description' => __( 'Rainbow Logo', 'basicmaths' )
		)
	) );
}
endif;

// Add Basic Maths <title> in header
function bm_conditional_title() {
	global $page, $paged;
	wp_title( '|', true, 'right' );

	// Add the blog name.
	bloginfo( 'name' );

	// Add the blog description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";

	// Add a page number if necessary:
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'twentyten' ), max( $paged, $page ) );
}

// Add Basic Maths Header styles for Admin
if ( ! function_exists( 'bm_admin_header_style' ) ) :
function bm_admin_header_style() {
global $options;
foreach ($options as $value) {
    if (get_option( $value['id'] ) === FALSE) { $$value['id'] = $value['std']; } else { $$value['id'] = get_option( $value['id'] ); }
} ?>
<style type="text/css">
/* Shows the same border as on front end */
.appearance_page_custom-header #headimg {border-width:0;padding:0;margin:0;background:#FFF;}
#headimg:hover {background-color:#<?php echo $bm_hover_color; ?>;}
#headimg h1 {height:148px;border:1px solid #<?php echo $bm_link_color; ?>;border-width:1px 0;position:relative;padding:0;margin:0;}
#headimg #name {position:absolute;bottom:14px;padding:0 10px;text-decoration:none;font-size:24px;line-height:24px;font-family:Helvetica,HelveticaNeue,Arial,Verdana,sans-serif;}
#headimg:hover #name,
#headimg h1 a#name[style] {color:#<?php echo $bm_link_color; ?> !important;}
#headimg h1:hover a#name[style] {color:#FFF !important;}
#headimg #desc {display:none;}
</style>
<?php
}
endif;

// Add Basic Math Colors
function bm_colors() { 
global $options;
foreach ($options as $value) {
    if (get_option( $value['id'] ) === FALSE) { $$value['id'] = $value['std']; } else { $$value['id'] = get_option( $value['id'] ); }
} ?>
<style type="text/css">
	/* <![CDATA[ */
	/* Basic Maths Colors */
<?php $bmcolors = dirname( __FILE__ ) . '/css/style.colors.php'; if( is_file( $bmcolors ) ) require $bmcolors; ?>
	/* ]]> */
</style>
<?php }
add_action('wp_head', 'bm_colors');

//	Basic Maths Navigation
function mytheme_nav() {
    if ( function_exists( 'wp_nav_menu' ) )
        wp_nav_menu( array( 'theme_location' => 'bm-menu', 'sort_column' => 'menu_order', 'depth' => '2', 'container_class' => 'bm-menu', 'fallback_cb' => 'bm_nav_fallback' ) );
    else
        bm_nav_fallback();
}

function bm_nav_fallback() {
    wp_page_menu( array( 'show_home' => __('Home', 'basicmaths'), 'sort_column' => 'menu_order', 'depth' => '2', 'container_class' => 'bm-menu' ) );
}

function bm_nav() {
	if (bm_is_iphone()) { ?>
		<div id="iphone-nav">
			<div id="iphone-menu">
				<span id="menu-hover"><a href="#"><?php _e( 'Menu', 'basicmaths' ) ?></a></span>
				<?php mytheme_nav(); ?>
			</div>
			<div id="nav-search">
				<label for="nav-s"><?php _e( 'Search', 'basicmaths' ) ?></label>
				<form id="nav-searchform" class="blog-search" method="get" action="<?php bloginfo('home') ?>">
					<div>
						<input id="nav-s" name="s" type="text" class="text" value="<?php the_search_query() ?>" tabindex="1" />
						<input id="nav-submit" name="submit" type="submit" class="button" value="<?php _e( 'Go', 'basicmaths' ) ?>" tabindex="2" />
					</div>
				</form>
			</div>
		</div>

<?php } else { ?>
		<div id="nav">
			<?php mytheme_nav(); ?>
			<div id="nav-search">
				<label for="nav-s"><?php _e( 'Search', 'basicmaths' ) ?></label>
				<form id="nav-searchform" class="blog-search" method="get" action="<?php bloginfo('home') ?>">
					<div>
						<input id="nav-s" name="s" type="text" class="text" value="<?php the_search_query() ?>" size="29" tabindex="1" />
						<input id="nav-submit" name="submit" type="submit" class="button" value="<?php _e( 'Go', 'basicmaths' ) ?>" tabindex="2" />
					</div>
				</form>
			</div>
		</div>
	
<?php } }

// Change Excerpt More Text [...]
function new_excerpt_more($more) {
	return '&hellip;';
}
add_filter('excerpt_more', 'new_excerpt_more');

// Page Paragraph column short code
// Right
function bm_rightcolumn($atts, $content = null) {
	return '<div class="rightcolumn"><p>' . do_shortcode($content) . '</p></div>';
}
add_shortcode("rightcolumn", "bm_rightcolumn");

// Top
function bm_topcolumn($atts, $content = null) {
	return '<div class="topcolumn"><p>' . do_shortcode($content) . '</p></div> ';
}
add_shortcode("topcolumn", "bm_topcolumn");

// Left
function bm_leftcolumn($atts, $content = null) {
	return '<div class="leftcolumn"><p>' . do_shortcode($content) . '</p></div> ';
}
add_shortcode("leftcolumn", "bm_leftcolumn");

// Hanging Columns Shortcode
// Hang Two Column
function bm_hang_2_column($atts, $content = null) {
	extract( shortcode_atts( array(
		'width' => 260,
		'element' => 'div',
		), $atts ) );
	return '<' . esc_attr($element) . ' class="hang-2-column" style="width:' . esc_attr($width) . 'px;">' . do_shortcode($content) . '</' . esc_attr($element) . '>';
}
add_shortcode("hang2column", "bm_hang_2_column");

// Hang One Column
function bm_hang_1_column($atts, $content = null) {
	extract( shortcode_atts( array(
		'width' => 260,
		'element' => 'div',
		), $atts ) );
	return '<' . esc_attr($element) . ' class="hang-1-column" style="width:' . esc_attr($width) . 'px;">' . do_shortcode($content) . '</' . esc_attr($element) . '>';
}
add_shortcode("hang1column", "bm_hang_1_column");

// No Hang
function bm_hang_0_column($atts, $content = null) {
	extract( shortcode_atts( array(
		'width' => 260,
		'element' => 'div',
		), $atts ) );
	return '<' . esc_attr($element) . ' class="no-hang" style="width:' . esc_attr($width) . 'px;">' . do_shortcode($content) . '</' . esc_attr($element) . '>';
}
add_shortcode("nohang", "bm_hang_0_column");

//	Reset Widgets
//update_option( 'sidebars_widgets', $null );

//	Count Posts
function bm_post_count() {
    global $wpdb;
	echo $wpdb->get_var("SELECT COUNT(*) FROM $wpdb->posts WHERE post_status = 'publish' AND post_date_gmt < '" . gmdate("Y-m-d H:i:s",time()) . "'");
}

//	Count Tags
function bm_tag_count() {
    $current_tag = get_query_var('tag_id');
	$mytags = get_terms( 'post_tag', array ('include' => $current_tag) );
	echo $mytags[0]->count;
}

//	Count Categories
function bm_cat_count() {
    $current_cat = get_query_var('cat');
	$mycats = get_terms( 'category', array ('include' => $current_cat) );
	echo $mycats[0]->count;
}

//	All Tags Count
function all_tags_count() {
	$alltags = get_tags();
	$count = count($alltags);
	echo $count;
}

//	Display Top Tags &/OR Categories Function
function basic_tags_categories() {
global $options;
foreach ($options as $value) {
    if (get_option( $value['id'] ) === FALSE) { $$value['id'] = $value['std']; } else { $$value['id'] = get_option( $value['id'] ); }
}
	
	if (!bm_is_iphone()) { ?>
	<?php if($bm_cats_or_tags == 'true') { ?>
			<div id="toptags" class="taxonomy-archive">
				<h3><?php _e( 'Categories', 'basicmaths' ) ?></h3>
				<ul>
					<?php basic_categories(); ?>
				</ul>
			</div>
	<?php } else { ?>
			<div id="toptags" class="taxonomy-archive">
				<h3><?php _e( 'Top Tags', 'basicmaths' ) ?></h3>
				<ul>
					<?php basic_tags(); ?>
				</ul>
			</div>
	<?php } } ?>
<?php }

//	Display Basic(Top) Categories
function show_basic_categories() { 
	if (!bm_is_iphone()) { ?>
	<div id="toptags" class="taxonomy-archive">
		<h3><?php _e( 'Categories', 'basicmaths' ) ?></h3>
		<ul>
			<?php basic_categories(); ?>
		</ul>
	</div>
<?php } }

//	Display Basic(Top) Tags
function show_basic_tags() { 
	if (!bm_is_iphone()) { ?>
	<div id="toptags" class="taxonomy-archive">
		<h3><?php _e( 'Top Tags', 'basicmaths' ) ?></h3>
		<ul>
			<?php basic_tags(); ?>
		</ul>
	</div>
<?php } }

//	Top Tags List w/ Count
function basic_tags() {
	global $options;
	foreach ($options as $value) {
	    if (get_option( $value['id'] ) === FALSE) { $$value['id'] = $value['std']; } else { $$value['id'] = get_option( $value['id'] ); }
	}
	
	$toptags = get_terms( 'post_tag', array ('fields' => 'ids', 'orderby' => 'count', 'order' => 'DESC', 'number' => $bm_top_tag_count, 'hierarchical' => false ) );
	$alltags = get_tags();
	$count = count($alltags);
	
	// Remeber the top tags
	// Thanks to the Extended Category Widget http://blog.avirtualhome.com/wordpress-plugins
	$included_tags = implode( ',', $toptags );

	// Only include the top categories
	$bm_listtags = get_tags( array('include' => $included_tags, 'orderby' => 'name', 'order' => 'ASC') );

	foreach ( (array) $bm_listtags as $tag ) {
		echo '<li><a href="' . get_tag_link ($tag->term_id) . '" rel="tag"><span>' . $tag->name . '</span> <span class="postcount">' . $tag->count . '</span></a></li>';
		echo "\n\t\t\t\t";
	}
	if($bm_archives_link == '') { 
		echo '<li class="all-tags-link"><span>' . __('All Tags', 'basicmaths') . '</span> <span class="postcount">';
		echo $count;
		echo "</span></li>\n";
	} else {
		$alltags_archives_link = '<a href="' . get_option('home') . '/' . $bm_archives_link . '">';
		echo '<li>' . $alltags_archives_link . '<span>' . __('All Tags', 'basicmaths') . '</span> <span class="postcount">';
		echo $count;
		echo "</span></a></li>\n";
	}
}

//	All Tags List w/ Count ' . $tag->tag_description . '
function all_basic_tags() {
	$tags = get_tags( array('orderby' => 'name', 'order' => 'ASC', 'number' => 0) );
	foreach ( (array) $tags as $tag ) {
		echo '<li><a href="' . get_tag_link ($tag->term_id) . '" rel="tag"><span>' . $tag->name . '</span> <span class="postcount">' . $tag->count . '</span></a></li>';
		echo "\n\t\t\t\t";
	}
}

//	All Categories Count
function all_categories_count() {
	$allcategories = get_categories();
	$count = count($allcategories);
	echo $count;
}

//	Top Categories List w/ Count
function basic_categories() {
	global $options;
	foreach ($options as $value) {
	    if (get_option( $value['id'] ) === FALSE) { $$value['id'] = $value['std']; } else { $$value['id'] = get_option( $value['id'] ); }
	}
	
	$topcats = get_terms( 'category', array ('fields' => 'ids', 'orderby' => 'count', 'order' => 'DESC', 'number' => $bm_top_tag_count, 'hierarchical' => false ) );
	$allcats = get_categories();
	$count = count($allcats);
	
	// Remeber the top categories
	// Thanks to the Extended Category Widget http://blog.avirtualhome.com/wordpress-plugins
	$included_cats = implode( ",", $topcats );

	// Only include the top categories
	$bm_listcats = get_categories( array('include' => $included_cats, 'orderby' => 'name', 'order' => 'ASC') );

	foreach ( (array) $bm_listcats as $cat ) {
		echo '<li><a href="' . get_category_link ($cat->term_id) . '" rel="tag"><span>' . $cat->name . '</span> <span class="postcount">' . $cat->count . '</span></a></li>';
		echo "\n\t\t\t\t";
	}
	if($bm_archives_link == '') { 
		echo '<li class="all-tags-link"><span>' . __('All Categories', 'basicmaths') . '</span> <span class="postcount">';
		echo $count;
		echo "</span></li>\n";
	} else {
		$allcats_archives_link = '<a href="' . get_option(home) . '/' . $bm_archives_link . '">';
		echo '<li>' . $allcats_archives_link . '<span>' . __('All Categories', 'basicmaths') . '</span> <span class="postcount">';
		echo $count;
		echo "</span></a></li>\n";
	}
}

//	All Categories List w/ Count 
function all_basic_categories() {
	$cats = get_categories(array('orderby' => 'name', 'order' => 'ASC', 'number' => 0) );
	foreach ( (array) $cats as $cat ) {
		echo '<li><a href="' . get_category_link ($cat->term_id) . '" rel="tag"><span>' . $cat->name . '</span> <span class="postcount">' . $cat->count . '</span></a>';
		$desc = $cat->description;
		if ($desc!='') { // The description is not empty
			echo ' <span class="description">' . $desc . '</span></li>';
		} else {
			echo ' <span class="description">' . __('No description entered yet.', 'basicmaths') . '</span></li>';
    	}
		echo "\n\t\t\t\t";
	}
}

//	Archive Count
function _category_count($input = '') {
	echo _get_category_count($input);
}
function _get_category_count($input = '') {
	global $wpdb;
	if($input == '')
	{
		$category = get_the_category();
		return $category[0]->category_count;
	}
	elseif(is_numeric($input))
	{
		$SQL = "SELECT $wpdb->term_taxonomy.count FROM $wpdb->terms, $wpdb->term_taxonomy WHERE $wpdb->terms.term_id=$wpdb->term_taxonomy.term_id AND $wpdb->term_taxonomy.term_id=$input";
		return $wpdb->get_var($SQL);
	}
	else
	{
		$SQL = "SELECT $wpdb->term_taxonomy.count FROM $wpdb->terms, $wpdb->term_taxonomy WHERE $wpdb->terms.term_id=$wpdb->term_taxonomy.term_id AND $wpdb->terms.slug='$input'";
		return $wpdb->get_var($SQL);
	}
}

//	Basic Date Archives 
//	http://wordpress.org/support/topic/227818
function basic_date_archives() {
	global $wpdb; ?>
<?php $years = $wpdb->get_col("SELECT DISTINCT YEAR( post_date ) FROM $wpdb->posts WHERE post_status = 'publish' AND post_type = 'post' ORDER BY post_date DESC"); foreach($years as $year) : ?>
		<div class="archive-year">
			<h4><a href="<?php bloginfo('url') ?>/?m=<?php echo $year; ?>"><?php echo $year; ?></a></h4>
			<ul>
<?php $months = $wpdb->get_results("SELECT DISTINCT MONTH( post_date ) AS month , YEAR( post_date ) AS year, COUNT( id ) as post_count FROM $wpdb->posts WHERE post_status = 'publish' AND post_type = 'post' AND YEAR(post_date) = '".$year."' GROUP BY month , year ORDER BY post_date DESC");
foreach($months as $month) : ?>
				<li><a href="<?php bloginfo('url') ?>/?m=<?php echo $month->year; ?><?php echo date("m", mktime(0, 0, 0, $month->month, 1, $month->year)) ?>"><?php echo date("F", mktime(0, 0, 0, $month->month, 1, $month->year)) ?> <span class="archiveyear"><?php echo $month->year ?></span> <span class="postcount"><?php echo $month->post_count; if ($month->post_count> 1 ) { echo __( ' entries', 'basicmaths' ); } else { echo __( ' entry', 'basicmaths' ); }?></span></a></li>
			
<?php endforeach;?>
			</ul>
		</div>
<?php endforeach; ?>

<?php }

//	Basic Date Archives
//	http://wordpress.org/support/topic/227818
function abbr_basic_date_arhives() { ?>
<?php global $wpdb; ?>
<?php $years = $wpdb->get_col("SELECT DISTINCT YEAR( post_date ) FROM $wpdb->posts WHERE post_status = 'publish' AND post_type = 'post' ORDER BY post_date DESC"); foreach($years as $year) : ?>
		<div class="archive-year">
			<ul>
<?php $months = $wpdb->get_results("SELECT DISTINCT MONTH( post_date ) AS month , YEAR( post_date ) AS year, COUNT( id ) as post_count FROM $wpdb->posts WHERE post_status = 'publish' AND post_type = 'post' AND YEAR(post_date) = '".$year."' GROUP BY month , year ORDER BY post_date DESC");
foreach($months as $month) : ?>
				<li><a href="<?php bloginfo('url') ?>/?m=<?php echo $month->year; ?><?php echo date("m", mktime(0, 0, 0, $month->month, 1, $month->year)) ?>"><?php echo date("M", mktime(0, 0, 0, $month->month, 1, $month->year)) ?> <span class="archiveyear"><?php echo $month->year ?></span> <span class="postcount"><?php echo $month->post_count; if ($month->post_count> 1 ) { echo __( ' entries', 'basicmaths' ); } else { echo __( ' entry', 'basicmaths' ); }?></span></a></li>
			
<?php endforeach;?>
			</ul>
		</div>
<?php endforeach; ?>

<?php
}

//	Basic Maths 404 Archive and email links
function basic_404_link() {
	global $options;
	foreach ($options as $value) {
	    if (get_option( $value['id'] ) === FALSE) { $$value['id'] = $value['std']; } else { $$value['id'] = get_option( $value['id'] ); }
	}

	if($bm_archives_link == '') {
		echo '<p>' . __('Still can’t find what you’re looking for? ', 'basicmaths') . ' <a href="mailto:' . antispambot(get_bloginfo('admin_email')) . '">' . __( 'Email me', 'basicmaths' ) . '</a>.' . __( 'Thanks!', 'basicmaths' ) . '</p>';

	} else {
		echo '<p>' . __( 'Still can’t find what you’re looking for? Try ', 'basicmaths' ) . '<a href="' . get_bloginfo ( 'url' ) . '/' . $bm_archives_link . '">' . __( 'browsing the archives', 'basicmaths' ) . '</a>, ' . __( 'or you can', 'basicmaths' ) . ' <a href="mailto:' . antispambot(get_bloginfo('admin_email')) . '">' . __( 'email me', 'basicmaths' ) . '</a>.' . __( 'Thanks!', 'basicmaths' ) . '</p>';

	}
}

//	Basic Maths Search Form
function basic_search_form($form) {
		$value = __('To search, type and hit enter', 'basicmaths');
		$form = '<form id="searchform" method="get" action="' . get_bloginfo('url') .'">
		<div><label class="hidden" for="s">' . __('Search for:', 'basicmaths') . '</label>
		<input id="s" name="s" type="text" value="' . $value . '" onfocus="if (this.value == \'' . $value . '\') {this.value = \'\';}" onblur="if (this.value == \'\') {this.value = \'' . $value . '\';}" size="24" tabindex="1" />
		<input id="searchsubmit" name="searchsubmit" type="submit" value="' . __('Go', 'basicmaths') . '" tabindex="2" />
		</div>
		</form>';
		return $form;
	}
add_filter('get_search_form', 'basic_search_form');

//	Register Sidebars
function init_bm_sidebars() {
	// Left Sidebar
	register_sidebar(array(
        'name' => 'Left Sidebar',
        'id' => 'left-sidebar',
		'before_widget'  =>   "\n\t\t\t" . '<li id="%1$s" class="widget %2$s">',
		'after_widget'   =>   "\n\t\t\t</li>\n",
		'before_title'   =>   "\n\t\t\t\t". '<h3 class="widgettitle">',
		'after_title'    =>   "</h3>\n"
    ));

	// Right Sidebar
	register_sidebar(array(
        'name' => 'Right Sidebar',
        'id' => 'right-sidebar',
		'before_widget'  =>   "\n\t\t\t" . '<li id="%1$s" class="widget %2$s">',
		'after_widget'   =>   "\n\t\t\t</li>\n",
		'before_title'   =>   "\n\t\t\t\t". '<h3 class="widgettitle">',
		'after_title'    =>   "</h3>\n"
    ));
    
	// Check for a Basic Maths widgets directory to add and activate additional widgets
	// Thanks to Joern Kretzschmar & Thematic http://themeshaper.com/thematic
	$widgets_dir = @ dir(ABSPATH . '/wp-content/themes/' . get_template() . '/widgets');
	if ($widgets_dir)	{
		while(($widgetFile = $widgets_dir->read()) !== false) {
			if (!preg_match('|^\.+$|', $widgetFile) && preg_match('|\.php$|', $widgetFile))
				include(ABSPATH . '/wp-content/themes/' . get_template() . '/widgets/' . $widgetFile);
		}
	}
	// Check for the child themes widgets directory to add and activate additional widgets
	// Thanks to Joern Kretzschmar & Thematic http://themeshaper.com/thematic
	$widgets_dir = @ dir(ABSPATH . '/wp-content/themes/' . get_stylesheet() . '/widgets');
	if ((TEMPLATENAME != THEMENAME) && ($widgets_dir)) {
		while(($widgetFile = $widgets_dir->read()) !== false) {
			if (!preg_match('|^\.+$|', $widgetFile) && preg_match('|\.php$|', $widgetFile))
				include(ABSPATH . '/wp-content/themes/' . get_stylesheet() . '/widgets/' . $widgetFile);
		}
	}

	// Debug: Makes sure no widgets are registered already. If so, be gone.
	// update_option( 'sidebars_widgets', NULL );
	
	$current_theme = get_option( 'template' ); // variable stores the current theme
	$target_theme = 'basicmaths'; // variable stores the theme we want to target
	$debug = false; // skip the ladie, da.
	
	// this checks to see if the user is trying to change themes, if so,
	// compare that theme with our target theme.
	if ( isset( $_GET['activated'] ) && $current_theme == $target_theme || $debug ) {
		update_option( 'widget_basic-maths-recent-posts', array( 2 => array( 'title' => 'Recent Posts' ), '_multiwidget' => 1 ) );
		update_option( 'widget_basic-maths-more-info', array( 3 => array( 'title' => 'More Info' ), '_multiwidget' => 1 ) );
		update_option( 'widget_basic-maths-archives', array( 4 => array( 'title' => 'Archives' ), '_multiwidget' => 1 ) );
		
		update_option( 'sidebars_widgets', array(
			'left-sidebar' => array(
				'basic-maths-archives-4',
			),
			'right-sidebar' => array(
				'basic-maths-recent-posts-2',
				'basic-maths-more-info-3',
			),
			'wp_inactive_widgets' => array(),
			'array_version' => 3
		));
	}
}
add_action( 'widgets_init', 'init_bm_sidebars' );	

// Check for static widgets in widget-ready areas
function is_sidebar_active( $index ){
	global $wp_registered_sidebars;
	
	$widgetcolums = wp_get_sidebars_widgets();
	if ($widgetcolums[$index]) return true;
	return false;
}

// Set the content width
if ( ! isset( $content_width ) )
	$content_width = 620;

// Custom Settings initiated at activation
if ( is_admin() && isset($_GET['activated'] ) && $pagenow == 'themes.php' ) {
	update_option('thumbnail_size_h', '170');
	update_option('thumbnail_size_w', '170');
	update_option('thumbnail_crop', '1');

	update_option('medium_size_h', '440');
	update_option('medium_size_w', '440');
	update_option('medium_crop', '0');

	update_option('large_size_h', '620');
	update_option('large_size_w', '620');
	update_option('large_crop', '0');

	// Set Nested/Threaded comment depth amount
	update_option('thread_comments', '1');
	update_option('thread_comments_depth', '3');

}

// Custom callback to list comments in Basic Maths style
function basic_comments($comment, $args, $depth) {
   $GLOBALS['comment'] = $comment; ?>
	<li id="li-comment-<?php comment_ID() ?>" <?php comment_class(); ?>>
		<div id="comment-<?php comment_ID(); ?>">
			<div class="comment-author vcard">
<?php if (!bm_is_iphone()) { ?> 
				<?php echo get_avatar($comment, $size = '48', $default = '' ); ?>
<?php } ?>				
				<?php
				$auth = get_comment_author();
				if ( 'admin' == $auth && !bm_is_iphone() ) {
					print '<span class="comment-author-type">' . __( 'Author', 'basicmaths' ) . '</span>';
				}
				?>
				<?php printf(__('<cite class="fn">%s</cite>, '), get_comment_author_link()) ?>
			</div>

			<div class="comment-meta commentmetadata">
				<a href="<?php esc_attr_e( get_comment_link( $comment->comment_ID ) ); ?>"><?php printf( __( '<span class="comment-date">%1$s</span> at <span class="comment-time">%2$s</span>' ), get_comment_date( get_option( 'date_format') ),  get_comment_time() ); ?></a><?php edit_comment_link( __( 'Edit', 'basicmaths' ), ' <span class="meta-sep">|</span> <span class="edit-link">', '</span>' ); ?>
			</div>
<?php if ($comment->comment_approved == '0') : ?>
			<div class="comment-meta comment-awaiting-moderation">
				<em><?php _e('Your comment is awaiting moderation.') ?></em>
			</div>
<?php endif; ?>

			<?php comment_text() ?>

			<div class="reply">
				<?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
				
			</div>
		</div>
<?php }

// Custom callback to list pings in Basic Maths style
function basic_pings($comment, $args, $depth) {
       $GLOBALS['comment'] = $comment;
        ?>
    		<li id="comment-<?php comment_ID() ?>" <?php comment_class() ?>>
    			<div class="comment-author"><?php printf(__('<span class="trackback-date">%2$s</span> %1$s', 'basicmaths'),
    					get_comment_author_link(),
    					get_comment_date(__('M j')) );
    					edit_comment_link(__('Edit', 'basicmaths'), ' <span class="meta-sep">|</span> <span class="edit-link">', '</span>'); ?></div>
    <?php if ($comment->comment_approved == '0') _e('\t\t\t\t\t<span class="unapproved">Your trackback is awaiting moderation.</span>\n', 'basicmaths') ?>
<?php }

//	Allowed tags in comments 
define('BM_CUSTOM_TAGS', true);
global $allowedtags;
	$allowedtags = array(
		'a' => array(
			'href' => array (),
			'title' => array ()),
		'abbr' => array(
			'title' => array ()),
		'acronym' => array(
			'title' => array ()),
		'b' => array(),
		'blockquote' => array(
			'cite' => array ()),
		//	'br' => array(),
		'cite' => array (),
		'code' => array(),
		'del' => array(
			'datetime' => array ()),
		//	'dd' => array(),
		//	'dl' => array(),
		//	'dt' => array(),
		'em' => array (), 'i' => array (),
		//	'ins' => array('datetime' => array(), 'cite' => array()),
		'ol' => array(),
		'ul' => array(),
		'li' => array(),
		//	'p' => array(),
		//	'q' => array('cite' => array ()),
		//'strike' => array(),
		'strong' => array(),
		//	'sub' => array(),
		//	'sup' => array(),
		//	'u' => array(),
	);
	
//	Add Basic Maths stylesheet to the WYSIWYG editor
//	- http://blog.estherswhite.net/2009/11/customizing-tinymce/
function bm_editor_style($url) {
	if ( !empty($url) )
		$url .= ',';

		// Change the path here if using sub-directory
		$url .= trailingslashit( get_template_directory_uri() ) . '/css/style.editor.css';
		return $url;
}
add_filter('mce_css', 'bm_editor_style');

//	Add style selector for TinyMCE Buttons
//	- Filter: http://blog.estherswhite.net/2009/11/customizing-tinymce/ 
//	- http://arfore.com/2009/07/09/modifying-the-tinycme-in-wordpress/
function bm_mce_btns($orig) {
	return array('formatselect', 'styleselect', '|', 'underline', 'justifyfull', 'forecolor',  '|', 'pastetext', 'pasteword', 'removeformat', '|', 'media', 'charmap', '|', 'outdent', 'indent', '|', 'undo', 'redo', 'wp_help' );
}
add_filter( 'mce_buttons_2', 'bm_mce_btns', 999 );

//	Add options for TinyMCE style selector
function bm_tiny_mce_before_init( $init_array ) {
	$init_array['theme_advanced_styles'] = "Hang 2 Columns=hang-2-column;Hang 1 Column=hang-1-column";
	return $init_array;
}
add_filter( 'tiny_mce_before_init', 'bm_tiny_mce_before_init' );

//	Basic Maths Options
//	------------------------------------------------------------------------------

$themename = "Basic Maths";
$shortname = "bm";
$options = array (

				array(	"name" => __('Link Color','basicmaths'),
						"desc" => __('Change the color of links, backgrounds and borders by entering a HEX color number. (ie: <span style="font-family:Monaco,Lucida Console,Courier,monospace;">003333</span>)','basicmaths'),
						"id" => $shortname."_link_color",
						"std" => "000033",
						"type" => "colorpicker"),

				array(	"name" => __('Hover Color','basicmaths'),
						"desc" => __('Change the color of hover links by entering a HEX color number. (ie: <span style="font-family:Monaco,Lucida Console,Courier,monospace;">FF6600</span>)','basicmaths'),
						"id" => $shortname."_hover_color",
						"std" => "FF6600",
						"type" => "colorpicker"),

				array(	"name" => __('Top Tag Count','basicmaths'),
						"desc" => __('Choose the number of &lsquo;Top Tags&rsquo; to display in the header (Set to <span style="font-family:Monaco,Lucida Console,Courier,monospace;">0</span> to show all tags).','basicmaths'),
						"id" => $shortname."_top_tag_count",
						"std" => "11",
						"type" => "text"),

				array(	"name" => __('Show Categories','basicmaths'),
						"desc" => __("Check here to display a list of Categories in the header instead of Tags.",'basicmaths'),
						"id" => $shortname."_cats_or_tags",
						"std" => "false",
						"type" => "checkbox"),

				array(	"name" => __('Archives Link','basicmaths'),
						"desc" => __('Insert the slug of the page using the Archives template. (example: For a page Titled “History” that uses the Archives template, insert the slug <span style="font-family:Monaco,Lucida Console,Courier,monospace;font-weight:600">history</span> so that the “All Tags” link points to the correct page. This only works with Pretty Permalinks: http://goo.gl/5Ii2).','basicmaths'),
						"id" => $shortname."_archives_link",
						"std" => "",
						"type" => "text"),

				array(	"name" => __('Footer Text','basicmaths'),
						"desc" => __('Type in the HTML text that will appear in the bottom of your footer. (example: &copy;2010 < a href="http://basicmaths.subtraction.com" >Your Copyright Info< /a >. All rights reserved.','basicmaths'),
						"id" => $shortname."_footer_text",
						"std" => "&copy;2010 <a href=\"http://basicmaths.subtraction.com\">Your Copyright Info</a>. All rights reserved.",
						"type" => "textarea",
						"options" => array(	"rows" => "3", "cols" => "94") ),
);

function basicmaths_add_admin() {
	global $themename, $shortname, $options;
	if ( isset($_GET['page']) && $_GET['page'] == basename(__FILE__) ) {
		if ( isset( $_REQUEST['action'] ) && $_REQUEST['action'] == 'save' ) {
			foreach ($options as $value) {
				update_option( $value['id'], $_REQUEST[ $value['id'] ] ); }
			foreach ($options as $value) {
				if( isset( $_REQUEST[ $value['id'] ] ) ) { update_option( $value['id'], $_REQUEST[ $value['id'] ]  ); } else { delete_option( $value['id'] ); } }
			header("Location: themes.php?page=functions.php&saved=true");
			die;
		} elseif ( isset( $_REQUEST['action'] ) && $_REQUEST['action'] == 'reset' ) {
			foreach ($options as $value) {
				delete_option( $value['id'] ); }
			header("Location: themes.php?page=functions.php&reset=true");
			die;
		}
	}
	add_theme_page($themename." Options", "Basic Maths Options", 'edit_themes', basename(__FILE__), 'basicmaths_admin');
}

function basicmaths_admin() {

    global $themename, $shortname, $options;

    if ( isset( $_REQUEST['saved'] ) ) echo '<div id="message" class="updated fade"><p><strong>'.$themename.' settings saved.</strong></p></div>';
    if ( isset( $_REQUEST['reset'] ) ) echo '<div id="message" class="updated fade"><p><strong>'.$themename.' settings reset.</strong></p></div>';
    
?>
<script language="javascript" type="text/javascript" src="<?php echo bloginfo('template_directory') ?>/js/jscolor/jscolor.js"></script>
<div class="wrap">
<?php if ( function_exists('screen_icon') ) screen_icon(); ?>
<h2><?php echo $themename; ?> Options</h2>

<form method="post" action="">

	<table class="form-table">

<?php foreach ($options as $value) { 
	
	switch ( $value['type'] ) {
		case 'text':
		?>
		<tr valign="top"> 
			<th scope="row"><label for="<?php echo $value['id']; ?>"><?php echo __($value['name'],'basicmaths'); ?></label></th>
			<td>
				<input name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" type="<?php echo $value['type']; ?>" value="<?php if ( get_option( $value['id'] ) != "") { echo get_option( $value['id'] ); } else { echo $value['std']; } ?>" />
				<?php echo __($value['desc'],'basicmaths'); ?>

			</td>
		</tr>
		<?php
		break;
		
		case 'colorpicker':
		?>
		<tr valign="top"> 
			<th scope="row"><label for="<?php echo $value['id']; ?>"><?php echo __($value['name'],'basicmaths'); ?></label></th>
			<td>
				<input type="<?php echo $value['type']; ?>" name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" value="<?php if ( get_option( $value['id'] ) != "") { echo get_option( $value['id'] ); } else { echo $value['std']; } ?>" class="color {pickerPosition:'right'}" />
				<?php echo __($value['desc'],'basicmaths'); ?>

			</td>
		</tr>
		<?php
		break;
		
		case 'select':
		?>
		<tr valign="top">
			<th scope="row"><label for="<?php echo $value['id']; ?>"><?php echo __($value['name'],'basicmaths'); ?></label></th>
				<td>
					<select name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>">
					<?php foreach ($value['options'] as $option) { ?>
					<option<?php if ( get_option( $value['id'] ) == $option) { echo ' selected="selected"'; } elseif ($option == $value['std']) { echo ' selected="selected"'; } ?>><?php echo $option; ?></option>
					<?php } ?>
				</select>
			</td>
		</tr>
		<?php
		break;
		
		case 'textarea':
		$ta_options = $value['options'];
		?>
		<tr valign="top"> 
			<th scope="row"><label for="<?php echo $value['id']; ?>"><?php echo __($value['name'],'basicmaths'); ?></label></th>
			<td><textarea name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" cols="<?php echo $ta_options['cols']; ?>" rows="<?php echo $ta_options['rows']; ?>"><?php 
				if( get_option($value['id']) != "") {
						echo __(stripslashes(get_option($value['id'])),'basicmaths');
					}else{
						echo __($value['std'],'basicmaths');
				}?></textarea><br /><?php echo __($value['desc'],'basicmaths'); ?></td>
		</tr>
		<?php
		break;

		case 'radio':
		?>
		<tr valign="top"> 
			<th scope="row"><?php echo __($value['name'],'basicmaths'); ?></th>
			<td>
				<?php foreach ($value['options'] as $key=>$option) { 
				$radio_setting = get_option($value['id']);
				if($radio_setting != ''){
					if ($key == get_option($value['id']) ) {
						$checked = "checked=\"checked\"";
						} else {
							$checked = "";
						}
				}else{
					if($key == $value['std']){
						$checked = "checked=\"checked\"";
					}else{
						$checked = "";
					}
				}?>
				<input type="radio" name="<?php echo $value['id']; ?>" id="<?php echo $value['id'] . $key; ?>" value="<?php echo $key; ?>" <?php echo $checked; ?> /><label for="<?php echo $value['id'] . $key; ?>"><?php echo $option; ?></label><br />
				<?php } ?>
			</td>
		</tr>
		<?php
		break;
		
		case 'checkbox':
		?>
		<tr valign="top"> 
			<th scope="row"><?php echo __($value['name'],'basicmaths'); ?></th>
			<td>
				<?php
					if(get_option($value['id'])){
						$checked = "checked=\"checked\"";
					}else{
						$checked = "";
					}
				?>
				<input type="checkbox" name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" value="true" <?php echo $checked; ?> />
				<label for="<?php echo $value['id']; ?>"><?php echo __($value['desc'],'basicmaths'); ?></label>
			</td>
		</tr>
		<?php
		break;

		default:

		break;
		
	}
}
?>

	</table>

	<p class="submit">
		<input name="save" type="submit" value="<?php _e('Save changes','basicmaths'); ?>" />    
		<input type="hidden" name="action" value="save" />
	</p>
</form>
<form method="post" action="">
	<p class="submit">
		<input name="reset" type="submit" value="<?php _e('Reset','basicmaths'); ?>" />
		<input type="hidden" name="action" value="reset" />
	</p>
</form>

<p><?php _e('For more information about this theme, visit the <a href="http://basicmaths.subtraction.com/" target="_blank">Basic Maths Theme Page</a>.', 'basicmaths'); ?></p>
</div>
<?php
}
add_action('admin_menu' , 'basicmaths_add_admin');

//	New Meta Boxes For Post ShortCodes
function post_shortcode_instructions() { ?>
	<div style="display:table;margin-bottom:40px;width:100%;">
		<h3>Example</h3>
		<p>Usage: <code>[hang1column element="div" width="200"]This is some hanging content.[/hang1column]</code></p>
	</div>
	<ul style="display:table">
		<li style="width:45%;height:200px;margin-right:10px;float:left;">
			<h3 style="margin-bottom:0;">[hang1column]</h3>
			<p>A wrapping element for floating content 1 column to the left.</p>
			<p>Usage: <code>[hang1column foo="bar"]%content%[/hang1column]</code></p>
			<ul style="list-style:disc;list-style-position:inside;"><p>Parameters:</p>
				<li>element="div" (Determines which HTML element to use: div, ul, ol, span, etc. Set to div by default.)</li>
				<li>width="260" (Default width in pixels)</li>
			</ul>
		</li>
		<li style="width:45%;height:200px;margin-right:10px;float:left;">
			<h3 style="margin-bottom:0;">[hang2column]</h3>
			<p>A wrapping element for floating content 2 columns to the left.</p>
			<p>Usage: <code>[hang2column foo="bar"]%content%[/hang2column]</code></p>
			<ul style="list-style:disc;list-style-position:inside;"><p>Parameters:</p>
				<li>element="div" (Determines which HTML element to use: div, ul, ol, span, etc. Set to div by default.)</li>
				<li>width="260" (Default width in pixels)</li>
			</ul>
		</li>
		<li style="width:45%;height:200px;margin-right:10px;float:left">
			<h3 style="margin-bottom:0;">[nohang]</h3>
			<p>A wrapping element for adding content with a fixed width without hanging.</p>
			<p>Usage: <code>[nohang foo="bar"]%content%[/nohang]</code></p>
			<ul style="list-style:disc;list-style-position:inside;"><p>Parameters:</p>
				<li>element="div" (Determines which HTML element to use: div, ul, ol, span, etc. Set to div by default.)</li>
				<li>width="260" (Default width in pixels)</li>
			</ul>
		</li>
	</ul>
<?php }

//	Basic Maths Meta Boxes For Page Shortcodes
function page_shortcode_instructions() { ?>
	<ul style="display:table;width:100%;">
		<li style="">
			<h3 style="margin-bottom:0;">[topcolumn]</h3>
			<p>Use this shortcode to display information at the full width of the Basic Maths layout.</p>
			<p>Usage: <code>[topcolumn]This is some paragraph text[/topcolumn]</code></p>
		</li>
		<li style="">
			<h3 style="margin-bottom:0;">[leftcolumn]</h3>
			<p>Use this shortcode to display information on the left at half the width of the content area.</p>
			<p>Usage: <code>[leftcolumn]This is some paragraph text[/leftcolumn]</code></p>
		</li>
		<li style="">
			<h3 style="margin-bottom:0;">[rightcolumn]</h3>
			<p>Use this shortcode to display information on the right at half the width of the content area.</p>
			<p>Usage: <code>[rightcolumn]This is some paragraph text[/rightcolumn]</code></p>
		</li>
	</ul>
<?php }

function create_meta_box() {
	add_meta_box( 'post_shortcode_instructions', 'Using Basic Maths’ Shortcodes', 'post_shortcode_instructions', 'post', 'normal', 'high' );  
	add_meta_box( 'page_shortcode_instructions', 'Using Basic Maths’ Shortcodes', 'page_shortcode_instructions', 'page', 'normal', 'high' );  
}
add_action('admin_menu', 'create_meta_box');

// Test for iPhone Browser
function bm_is_iPhone() {
  $agents = array(
    'iPhone',
    'iPod'
  );
  foreach ($agents as $agent) {
    if (strstr($_SERVER["HTTP_USER_AGENT"], $agent) or isset($_GET[$agent]) && $_GET[$agent] ) {
        return true;
    }
  }
  return false;
}

// Test for IE6 Browser 
// DON’T FORGET TO TEST FOR ALL IE BROWSERS HERE NOT JUST IE6 !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
function bm_is_ie6() {
	if(strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 6') !== false) {
		return true;
	}
}

//	Body Browser Classes
function browser_body_class($classes) {
	global $is_lynx, $is_gecko, $is_IE, $is_opera, $is_NS4, $is_safari, $is_chrome, $is_iphone;

	$browser = $_SERVER[ 'HTTP_USER_AGENT' ];
	
	// Mac, PC ...or Linux
	if ( preg_match( "/Mac/", $browser ) ){
			$classes[] = 'mac';
		
	} elseif ( preg_match( "/Windows/", $browser ) ){
			$classes[] = 'windows';
		
	} elseif ( preg_match( "/Linux/", $browser ) ) {
			$classes[] = 'linux';

	} else {
			$classes[] = 'unknown-os';
	}
	
	// Checks browsers in this order: Chrome, Safari, Opera, MSIE, FF
	if ($is_ipad = (bool) strpos($_SERVER['HTTP_USER_AGENT'],'iPad') ) {
			$classes[] = 'ipad';

	} elseif ($is_iphone) {
			$classes[] = 'iphone';
			
	} elseif ( preg_match( "/Chrome/", $browser ) ) {
			$classes[] = 'chrome';

			preg_match( "/Chrome\/(\d.\d)/si", $browser, $matches);
			$ch_version = 'ch' . str_replace( '.', '-', $matches[1] );      
			$classes[] = $ch_version;

	} elseif ( preg_match( "/Safari/", $browser ) ) {
			$classes[] = 'safari';
			
			preg_match( "/Version\/(\d.\d)/si", $browser, $matches);
			$sf_version = 'sf' . str_replace( '.', '-', $matches[1] );      
			$classes[] = $sf_version;
			
	} elseif ( preg_match( "/Opera/", $browser ) ) {
			$classes[] = 'opera';
			
			preg_match( "/Opera\/(\d.\d)/si", $browser, $matches);
			$op_version = 'op' . str_replace( '.', '-', $matches[1] );      
			$classes[] = $op_version;
			
	} elseif ( preg_match( "/MSIE/", $browser ) ) {
			$classes[] = 'msie';
			
			if( preg_match( "/MSIE 6.0/", $browser ) ) {
					$classes[] = 'ie6';
			} elseif ( preg_match( "/MSIE 7.0/", $browser ) ){
					$classes[] = 'ie7';
			} elseif ( preg_match( "/MSIE 8.0/", $browser ) ){
					$classes[] = 'ie8';
			}
			
	} elseif ( preg_match( "/Firefox/", $browser ) && preg_match( "/Gecko/", $browser ) ) {
			$classes[] = 'firefox';
			
			preg_match( "/Firefox\/(\d)/si", $browser, $matches);
			$ff_version = 'ff' . str_replace( '.', '-', $matches[1] );      
			$classes[] = $ff_version;
			
	} else {
			$classes[] = 'unknown-browser';
	}

	return $classes;
}
add_filter('body_class','browser_body_class');

// On iPhone add some iPhone Meta styles
function bm_iphone_meta() {
	if (bm_is_iphone()) { ?>
		<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;"/>
	<?php }
}
add_action('wp_head', 'bm_iphone_meta');

//	Add Basic Maths Styles for Desktop and iPhone browsers
//	- Add a filter for child theme styles if needed
function bm_styles() {
	$content = wp_enqueue_style('bm-theme', STYLEURL, 'false', '1.1', 'all');

	if (!bm_is_iphone()) {
		$content .= wp_enqueue_style('bm-default', TEMPLATEDIR . '/css/style.default.css', 'bm-theme', '1.1', 'screen');
	} else {
		$content .= wp_enqueue_style('bm-iphone', TEMPLATEDIR . '/css/style.iphone.css', 'bm-theme', '1.1', 'screen');
	}
	
	if (bm_is_ie6()) {
		$content .= wp_enqueue_style('bm-iestyles', TEMPLATEDIR . '/css/style.ie.css', 'bm-theme', '1.1', 'screen');
	}
	
	// Add a filter for Child Theme Styles
    echo apply_filters('bm_styles', $content);
}
add_action('wp_print_styles', 'bm_styles');


// Enqueue JS
function bm_enqueue_scripts() {
	// Swap jQuery library when not in Admin areas
	if (!is_admin()) {
		wp_enqueue_script('jquery');
		
		// Enqueue jQuery Scripts
		if (!bm_is_iphone()) {
			wp_enqueue_script('hashgrid', TEMPLATEDIR . '/js/hashgrid.js', array('jquery'), '0.7.9' );
			wp_enqueue_script('robustcols', TEMPLATEDIR . '/js/robustcolumns.js', array('jquery'), '1.0' );
		} else {
			wp_enqueue_script('jqueryiphone', TEMPLATEDIR . '/js/jquery.iphone.min.js', array('jquery'), '0.1.2' );
		}
		
		if ( !bm_is_iphone() ) {
			wp_enqueue_script('bmscripts', TEMPLATEDIR . '/js/basicmaths.js', array('jquery','hashgrid','robustcols'), '1.1' );
		} else {
			wp_enqueue_script('bmiphonescripts', TEMPLATEDIR . '/js/basicmaths.iphone.js', array('jquery','jqueryiphone'), '1.1' );
		}
	}
}
add_action('wp_print_scripts', 'bm_enqueue_scripts');

?>