<?php 

class Basic_Maths_More_Info_Class extends WP_Widget {
	function Basic_Maths_More_Info_Class() {
		$widget_ops = array('classname' => 'widget_basic_maths_more_info', 'description' => 'A widget that displays subscribe links and a WordPress.org link formatted specifically for Basic Maths.' );
		$this->WP_Widget('basic-maths-more-info', 'Basic Maths More Info', $widget_ops);
	}

	function widget($args, $instance) {
		extract($args, EXTR_SKIP);

		echo $before_widget;
		$title = empty($instance['title']) ? 'More Info' : apply_filters('widget_title', $instance['title']);
		if ( !empty( $title ) ) { echo $before_title . $title . $after_title; }; ?>

				<ul>
					<li class="entries-rss"><a href="<?php bloginfo('rss2_url'); ?>"><?php _e( 'RSS Feed for Entries', 'basicmaths' ) ?></a></li>
					<li class="comments-rss"><a href="<?php bloginfo('comments_rss2_url'); ?>"><?php _e( 'RSS Feed for Comments', 'basicmaths' ) ?></a></li>
					<li class="wordpress-link"><a href="http://wordpress.org" target="_blank"><?php _e( 'Powered by WordPress', 'basicmaths' ) ?></a></li>
				</ul>

		<?php

		echo $after_widget;
	}
 
	function update($new_instance, $old_instance) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
 
		return $instance;
	}
 
	function form($instance) {
		$instance = wp_parse_args( (array) $instance, array( 'title' => '' ) );
		$title = strip_tags($instance['title']);
?>
			<p><label for="<?php echo $this->get_field_id('title'); ?>">Title: <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo attribute_escape($title); ?>" /></label></p>
<?php }
}
register_widget( 'Basic_Maths_More_Info_Class' );

?>