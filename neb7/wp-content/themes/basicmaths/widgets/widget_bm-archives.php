<?php 

class Basic_Maths_Archives_Class extends WP_Widget {
	function Basic_Maths_Archives_Class() {
		$widget_ops = array('classname' => 'widget_basic_maths_archives', 'description' => 'A widget that displays a date based archive formatted specifically for Basic Maths.' );
		$this->WP_Widget('basic-maths-archives', 'Basic Maths Archives', $widget_ops);
	}

	function widget($args, $instance) {
		extract($args, EXTR_SKIP);

		echo $before_widget;
		$title = empty($instance['title']) ? 'Archives' : apply_filters('widget_title', $instance['title']);
		if ( !empty( $title ) ) { echo $before_title . $title . $after_title; }; ?>

		<?php abbr_basic_date_arhives(); ?>

		<?php

		echo $after_widget;
	}
 
	function update($new_instance, $old_instance) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
 
		return $instance;
	}
 
	function form($instance) {
		$instance = wp_parse_args( (array) $instance, array( 'title' => '' ) );
		$title = strip_tags($instance['title']);
?>
			<p><label for="<?php echo $this->get_field_id('title'); ?>">Title: <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo attribute_escape($title); ?>" /></label></p>
<?php }
}
register_widget( 'Basic_Maths_Archives_Class' );

?>