<?php 

class Basic_Maths_Recent_Posts_Class extends WP_Widget {
	function Basic_Maths_Recent_Posts_Class() {
		$widget_ops = array('classname' => 'widget_basic_maths_recent_posts', 'description' => 'A widget that displays recent posts formatted specifically for Basic Maths.' );
		$this->WP_Widget('basic-maths-recent-posts', 'Basic Maths Recent Posts', $widget_ops);
	}

	function widget($args, $instance) {
		extract($args, EXTR_SKIP);

		echo $before_widget;
		$title = empty($instance['title']) ? 'Recent Posts' : apply_filters('widget_title', $instance['title']);
		if ( !empty( $title ) ) { echo $before_title . $title . $after_title; }; ?>

				<ul class="xoxo featured-video">
					<?php
					global $post;
					$myposts = get_posts('post_type=post&showposts=5');
					foreach($myposts as $post) :
					?>
						<li><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><span class="recent-post-date"><?php the_time('M j, ’y'); ?></span> <?php the_title(); ?></a></li>
					<?php endforeach; ?>
				</ul>

		<?php

		echo $after_widget;
	}
 
	function update($new_instance, $old_instance) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
 
		return $instance;
	}
 
	function form($instance) {
		$instance = wp_parse_args( (array) $instance, array( 'title' => '' ) );
		$title = strip_tags($instance['title']);
?>
			<p><label for="<?php echo $this->get_field_id('title'); ?>">Title: <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo attribute_escape($title); ?>" /></label></p>
<?php }
}
register_widget( 'Basic_Maths_Recent_Posts_Class' );

?>