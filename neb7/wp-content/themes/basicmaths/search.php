<?php
global $options;
foreach ($options as $value) {
    if (get_option( $value['id'] ) === FALSE) { $$value['id'] = $value['std']; }
    else { $$value['id'] = get_option( $value['id'] ); }
    }
?>
<?php get_header(); ?>
		
	<div id="container">

<?php basic_tags_categories(); ?>

<?php if ( have_posts() ) : ?>

			<h2 class="archive-title"><span class="search-term"><?php _e( 'Search term: ', 'basicmaths' ) ?></span><?php the_search_query() ?></h2>
			<div class="single-entry-meta archive-meta">
				<span class="meta-item">
					<span class="label"><?php _e( 'Entries', 'basicmaths' ) ?></span>
					<span class="post-count meta-content"><?php $allsearch = &new WP_Query("s=$s&showposts=-1"); $key = wp_specialchars($s, 1); $count = $allsearch->post_count; echo $count . ' '; wp_reset_query(); ?><?php _e(' Total', 'basicmaths') ?></span>
				</span>
			</div>

		<div id="content">

		<?php
		/* Run the loop for the search to output the results.
		 * If you want to overload this in a child theme then include a file
		 * called loop-search.php and that will be used instead.
		 */
		 get_template_part( 'loop', 'search' );
		?>

<?php else : ?>

		<div id="content">
			<div class="post">
				<h2 class="archive-title"><span class="error"><?php _e( 'Woops', 'basicmaths' ) ?></span> <?php _e( 'Nothing found', 'basicmaths' ) ?></h2>
				<div class="single-entry-meta">
					<span class="meta-item meta-message">
						<span class="label"><?php _e( 'Message', 'basicmaths' ) ?></span>
						<span class="meta-content"><?php _e( 'Sorry, but nothing matched your search criteria.<br />Please try another search term here:', 'basicmaths' ) ?></span>
					</span>

					<span class="meta-item meta-search">
						<label class="label" for="s"><?php _e( 'Search', 'basicmaths' ) ?></label>
						<span class="meta-content">
							<form id="searchform" class="blog-search" method="get" action="<?php bloginfo('home') ?>">
								<div>
									<input id="s" name="s" type="text" class="text" value="<?php the_search_query() ?>" size="30" tabindex="1" />
									<input type="submit" class="button" value="<?php _e( 'Go', 'basicmaths' ) ?>" tabindex="2" />
								</div>
							</form>
						</span>
					</span>

				</div><!-- .single-entry-meta -->

				<div class="entry-content">
<?php basic_404_link(); ?>

				</div><!-- .entry-content -->

			</div><!-- .post -->

<?php endif; ?>

		</div><!-- #content -->
	
<?php get_sidebar(); ?>

	</div><!-- #container -->

<?php get_footer(); ?>