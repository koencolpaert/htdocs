<?php
global $options;
foreach ($options as $value) {
    if (get_option( $value['id'] ) === FALSE) { $$value['id'] = $value['std']; }
    else { $$value['id'] = get_option( $value['id'] ); }
    }
?>
<?php get_header(); ?>
		
	<div id="container">

<?php if (!bm_is_iphone()) { ?>
		<div id="basic-maths-calendar">
			<?php get_calendar(false); ?>
		</div>
<?php } ?>

<?php the_post() ?>

<?php if ( is_day() ) : ?>
			<h2 class="archive-title"><span><?php _e( 'Day', 'basicmaths' ); ?></span> <?php the_time( get_option( 'date_format' ) ); ?></h2>

<?php elseif ( is_month() ) : ?>
			<h2 class="archive-title"><span><?php _e( 'Month', 'basicmaths' ); ?></span> <?php the_time( 'F Y' ); ?></h2>

<?php elseif ( is_year() ) : ?>
			<h2 class="archive-title"><span><?php _e( 'Year', 'basicmaths' ); ?></span> <?php the_time( 'Y' ); ?></h2>

<?php elseif ( isset($_GET['paged']) && !empty($_GET['paged']) ) : ?>
			<h2 class="archive-title"><?php _e( 'Archives', 'basicmaths' ); ?></h2>

<?php endif; ?>
			<div class="single-entry-meta archive-meta">
				<span class="author meta-item"><span class="label"><?php _e( 'Entries', 'basicmaths' ) ?></span> <span class="post-count meta-content"><?php echo $wp_query->post_count; ?><?php _e(' Total', 'basicmaths') ?></span></span>
			</div>

		<div id="content">
			
<?php
	/* Since we called the_post() above, we need to
	 * rewind the loop back to the beginning that way
	 * we can run the loop properly, in full.
	 */
	rewind_posts();

	/* Run the loop for the archives page to output the posts.
	 * If you want to overload this in a child theme then include a file
	 * called loop-archives.php and that will be used instead.
	 */
	 get_template_part( 'loop', 'archive' );
?>

		</div><!-- #content -->
	
<?php get_sidebar(); ?>

	</div><!-- #container -->

<?php get_footer(); ?>