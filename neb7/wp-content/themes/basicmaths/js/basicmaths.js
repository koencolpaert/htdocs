// Basic Maths Scripts
jQuery(document).ready(function(jQuery){
	// Top Tags JS
	jQuery('#toptags ul').makeacolumnlists({cols: 4, colWidth: 0, equalHeight: 'ul', startN: 1});
	jQuery('#alltags ul').makeacolumnlists({cols: 4, colWidth: 0, equalHeight: 'ul', startN: 1});
	jQuery('#allcategories ul').makeacolumnlists({cols: 4, colWidth: 0, equalHeight: 'ul', startN: 1});
	
	// Grid JS
//	jQuery(document).bind('keydown', 'Alt+Shift+g', function(){
//		jQuery("body").toggleClass("gridSystem");
//	});

	// Grid JS
	// Hashgrid - http://hashgrid.com/
	var grid = new hashgrid({
		id: 'bm-grid',            // id for the grid container
		modifierKey: 'alt',      // optional 'ctrl', 'alt' or 'shift'
		showGridKey: 'g',        // key to show the grid
		holdGridKey: 'h',    // key to hold the grid in place
		foregroundKey: 'f',      // key to toggle foreground/background
		jumpGridsKey: 'd',       // key to cycle through the grid classes
		numberOfGrids: 1,        // number of grid classes used
		classPrefix: 'bmgrid-',    // prefix for the grid classes
		cookiePrefix: 'bmgrid'   // prefix for the cookie name
	});

});
