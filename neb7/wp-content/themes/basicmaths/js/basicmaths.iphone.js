// Basic Maths Scripts for iPhone
jQuery(document).ready(function(jQuery){
	// Hide iPhone Menu
	jQuery("#iphone-nav #iphone-menu div").hide();

	// iPhone Menu
	jQuery('#menu-hover').click(function(){
		jQuery(this).toggleClass("active");
		jQuery(this).siblings("#iphone-nav #iphone-menu div").slideToggle("fast");
	});
	jQuery.iPhone.hideURLbar();
	jQuery.iPhone.disableTextSizeAdjust();
});
