<?php
global $options;
foreach ($options as $value) {
    if (get_option( $value['id'] ) === FALSE) { $$value['id'] = $value['std']; }
    else { $$value['id'] = get_option( $value['id'] ); }
    }
?>
<?php get_header(); ?>
		
	<div id="container">

<?php basic_tags_categories(); ?>

		<div id="content">
	
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
			
			<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<h2 class="entry-title"><?php the_title(); ?></h2>
				<div class="entry-date">
					<abbr class="date published" title="<?php the_time('Y-m-d') ?>"><?php the_time('M j, ’y'); ?></abbr>
					<abbr class="time published" title="<?php the_time('TH:i:sO') ?>"><?php the_time('g:i A'); ?></abbr>
				</div>
				
				<div class="entry-content">
				<p class="attachment"><a href="<?php echo wp_get_attachment_url($post->ID); ?>"><?php echo wp_get_attachment_image( $post->ID, 'full' ); ?></a></p>
				<div class="caption"><?php if ( !empty($post->post_excerpt) ) the_excerpt(); // this is the "caption" ?></div>

<?php the_content(''); ?>
				</div>
				
				<div class="entry-meta">
					<span class="continue-link"><a href="<?php echo get_permalink($post->post_parent); ?>" rev="attachment">&lsaquo; Back to full post</a></span>
					<span class="comments-link"><?php comments_popup_link( __( 'Comments (0)', 'basicmaths' ), __( 'Comments (1)', 'basicmaths' ), __( 'Comments (%)', 'basicmaths' ) ) ?></span>
					<?php edit_post_link(__( 'Edit', 'basicmaths' ), '<span class="edit">', '</span>'); ?> 
				</div><!-- .entry-meta -->

			</div><!-- .post -->
	
			<div class="nextprev pagination">
				<div class="nav-previous"><?php previous_image_link( array( 60, 60 ) ) ?></div>
				<div class="nav-next"><?php next_image_link( array( 60, 60 ) ) ?></div>
			</div><!-- .nextprev -->

<?php // comments_template('', true); ?>
	
<?php endwhile; else: ?>
		
			<h2><?php _e('Woops&hellip;', 'basicmaths') ?></h2>
			<p><?php _e('Sorry, no attachments we&rsquo;re found.', 'basicmaths') ?></p>
		
<?php endif; ?>
	
		</div><!-- #content -->

	</div><!-- #container -->

<?php get_footer(); ?>