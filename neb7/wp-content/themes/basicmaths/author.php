<?php
global $options;
foreach ($options as $value) {
    if (get_option( $value['id'] ) === FALSE) { $$value['id'] = $value['std']; }
    else { $$value['id'] = get_option( $value['id'] ); }
    }

$authordata = get_userdata( intval( $author ) );

?>
<?php get_header(); ?>
		
	<div id="container">

<?php basic_tags_categories(); ?>

<?php
	/* Queue the first post, that way we know who
	 * the author is when we try to get their name,
	 * URL, description, avatar, etc.
	 *
	 * We reset this later so we can run the loop
	 * properly with a call to rewind_posts().
	 */
	if ( have_posts() )
		the_post();
?>

		<h2 class="archive-title"><span><?php _e( 'Author', 'basicmaths' ) ?></span> <?php echo get_the_author(); ?></h2> 
		<div class="single-entry-meta archive-meta">
			<span class="meta-item"><span class="label"><?php _e( 'Entries', 'basicmaths' ) ?></span> <span class="post-count meta-content"><?php echo get_usernumposts($authordata->ID); ?><?php _e(' Total', 'basicmaths') ?></span></span>
			<span class="meta-item author-bio"><span class="label"><?php _e( 'Bio', 'basicmaths' ) ?></span> <p><?php the_author_meta('user_description', $authordata->ID); ?>&nbsp;</p></span>
			<span class="meta-item author-contact"><span class="label"><?php _e( 'Contact', 'basicmaths' ) ?></span> <span class="email-address meta-content"><a class="email" title="<?php echo antispambot($authordata->user_email); ?>" href="mailto:<?php echo antispambot($authordata->user_email); ?>"><span class="fn n"><?php _e('Email ', 'basicmaths') ?><span class="given-name"><?php echo $authordata->first_name; ?></span> <span class="family-name"><?php echo $authordata->last_name; ?></span></span></a></span></span>
		</div>

		<div id="content">
			
<?php
	/* Since we called the_post() above, we need to
	 * rewind the loop back to the beginning that way
	 * we can run the loop properly, in full.
	 */
	rewind_posts();

	/* Run the loop for the author archive page to output the authors posts
	 * If you want to overload this in a child theme then include a file
	 * called loop-author.php and that will be used instead.
	 */
	 get_template_part( 'loop', 'author' );
?>

		</div><!-- #content -->
	
<?php get_sidebar(); ?>

	</div><!-- #container -->

<?php get_footer(); ?>