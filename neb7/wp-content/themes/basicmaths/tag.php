<?php get_header() ?>

	<div id="container">

<?php show_basic_tags(); ?>

		<h2 class="archive-title"><span><?php _e( 'Tag', 'basicmaths' ) ?></span> <?php single_tag_title() ?></h2>
		<div class="single-entry-meta archive-meta">
			<span class="meta-item">
				<span class="label"><?php _e( 'Entries', 'basicmaths' ) ?> </span>
				<span class="post-count meta-content"><?php echo bm_tag_count(); ?><?php _e(' Total', 'basicmaths') ?></span>
				<?php $tagdesc = tag_description(); if ( !empty($tagdesc) ) echo apply_filters( 'archive_meta', '<span class="tag-description meta-item"><span class="label">' . __('Description', 'basicmaths') . '</span>' . $tagdesc . '</span>' ); ?>
			</span>
		</div>

		<div id="content">

<?php
/* Run the loop for the tag archive to output the posts
 * If you want to overload this in a child theme then include a file
 * called loop-tag.php and that will be used instead.
 */
 get_template_part( 'loop', 'tag' );
?>
		
	</div><!-- #content -->

<?php get_sidebar(); ?>

</div><!-- #container -->

<?php get_footer(); ?>