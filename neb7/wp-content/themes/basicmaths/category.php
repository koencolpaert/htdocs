<?php get_header(); ?>
		
	<div id="container">

<?php show_basic_categories(); ?>

			<h2 class="archive-title"><span><?php _e( 'Category ', 'basicmaths' ) ?></span><?php single_cat_title() ?></h2>
			<div class="single-entry-meta archive-meta">
				<span class="meta-item"><span class="label"><?php _e( 'Entries', 'basicmaths' ) ?></span> <span class="post-count meta-content"><?php echo bm_cat_count(); ?><?php _e(' Total', 'basicmaths') ?></span></span>
				<?php $categorydesc = category_description(); if ( !empty($categorydesc) ) echo apply_filters( 'archive_meta', '<span class="category-description meta-item"><span class="label">' . __('Description', 'basicmaths') . '</span>' . $categorydesc . '</span>' ); ?>
			</div>

		<div id="content">

				<?php
				/* Run the loop for the category page to output the posts.
				 * If you want to overload this in a child theme then include a file
				 * called loop-category.php and that will be used instead.
				 */
				get_template_part( 'loop', 'category' );
				?>

		</div><!-- #content -->
	
<?php get_sidebar(); ?>

	</div><!-- #container -->

<?php get_footer(); ?>