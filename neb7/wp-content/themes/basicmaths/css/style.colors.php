<?php 
global $options;
foreach ($options as $value) {
    if (get_option( $value['id'] ) === FALSE) { $$value['id'] = $value['std']; } else { $$value['id'] = get_option( $value['id'] ); }
} ?>
	a, a:link, a:visited, #basic-maths-calendar #wp-calendar tfoot a, .paged #nav li.current_page_item a, .bm-link-color {color:#<?php echo $bm_link_color; ?>;}
	a:hover, a:active, #nav li ul li.page_item a, #nav li ul li.menu-item a, #nav li.current_page_item ul li.page_item a, #nav li.current_page_item ul li.menu-item a, .sidebar #recentcomments a:hover, .sidebar .textwidget a:hover, .edit-link a:hover, #content a.post-edit-link:hover, .bm-hover-color {color:#<?php echo $bm_hover_color; ?>;}
	#header, #toptags, #alltags, #toptags ul li .description, #nav, .archive-meta, .page #content .entry-content .topcolumn, #wp-calendar tbody a, #wp-calendar, #header h1, #comments-list, #trackbacks-list, #comments-list li.byuser, #respond, #footer {border-color:#<?php echo $bm_link_color; ?>;}
	#nav li:hover a, #nav li ul, #nav li.page_item ul li ul, #nav li.menu-item ul li ul, .paged #nav li.current_page_item:hover, .paged #nav li.current_page_item:hover a, #alltags ul li .description, #basic-maths-calendar #wp-calendar a:hover {border-color:#<?php echo $bm_hover_color; ?>;}
	#nav {border-bottom-color:#AAA}
	#nav li ul li.page_item a, .nav-previous a {border-color:#AAA;}
	.edit-link a, #content a.post-edit-link {color:red;}
	#skip, #basic-maths-calendar #wp-calendar a {background-color:#<?php echo $bm_link_color; ?>;}
	#skip:hover, #header h1:hover, .paged #nav li.current_page_item:hover, .paged #nav li.current_page_item:hover a, .paged #iphone-nav li.current_page_item:hover, .paged #iphone-nav li.current_page_item:hover a, .taxonomy-archive ul li:hover, .taxonomy-archive ul li:hover, #datearchives ul li:hover, .taxonomy-archive ul li:hover, .taxonomy-archive ul li:hover a span, .taxonomy-archive ul li:hover a span, .taxonomy-archive ul li:hover a span, #datearchives ul li:hover a span, #nav li:hover, #iphone-nav li:hover, .nextprev a:hover, .sidebar ul li a:hover, .sidebar #basic-maths-recent-posts ul li a:hover, .sidebar #basic-maths-archives ul li a:hover, #basic-maths-calendar #wp-calendar a:hover, .active, .active a {background-color:#<?php echo $bm_hover_color; ?>;}
	#basic-maths-calendar #wp-calendar tfoot a {background:transparent;}
	
	/* Header and Logo CSS */
<?php if ( get_header_image() != '' ) { ?>
	#header h1 a {background:transparent url('<?php header_image(); ?>') center center no-repeat;}
	.iphone #header h1 a {background:transparent;}
<?php } ?>
<?php if ( 'blank' == get_header_textcolor() ) { ?>
	#header h1 a span {visibility:hidden;}
	.iphone #header h1 a span {visibility:visible;}
<?php } else { ?>
	#header h1 a {color:#<?php echo $bm_link_color; ?>;}

<?php } ?>