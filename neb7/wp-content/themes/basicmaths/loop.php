<?php
global $options;
foreach ($options as $value) {
    if (get_option( $value['id'] ) === FALSE) { $$value['id'] = $value['std']; }
    else { $$value['id'] = get_option( $value['id'] ); }
    }
?>

<?php /* If there are no posts to display */ ?>
<?php if ( ! have_posts() ) : ?>
			<div class="post-0" <?php post_class(); ?>>
				<h2 class="archive-title"><?php _e( 'Error', 'basicmaths' ) ?> <span class="error"><?php _e( '404', 'basicmaths' ) ?></span></h2>
				<div class="single-entry-meta">
					<span class="meta-item meta-description"><span class="label"><?php _e( 'Description', 'basicmaths' ) ?></span> <span class="meta-content error"><?php _e( 'File not found', 'basicmaths' ) ?></span></span>
					<span class="meta-item meta-message"><span class="label"><?php _e( 'Message', 'basicmaths' ) ?></span> <span class="meta-content"><?php _e( 'Sorry, the page that you were looking for could not be found. If you want to find something else, you can search for it here:', 'basicmaths' ) ?></span></span>
					<span class="meta-item meta-search"><label class="label" for="s"><?php _e( 'Search', 'basicmaths' ) ?></label>
						<span class="meta-content">
							<form id="searchform" class="blog-search" method="get" action="<?php bloginfo('home') ?>">
								<div>
									<input id="s" name="s" type="text" class="text" value="<?php the_search_query() ?>" size="30" tabindex="1" />
									<input type="submit" class="button" value="<?php _e( 'Go', 'basicmaths' ) ?>" tabindex="2" />
								</div>
							</form>
						</span>
					</span>
				</div><!-- .single-entry-meta -->
				
				<div class="entry-content">
					<?php basic_404_link(); ?>
				</div><!-- .entry-content -->

			</div><!-- .post -->
<?php endif; ?>

<?php
	/* Start the Loop.
	 *
	 * In Basic Maths we use the same loop in multiple contexts.
	 */ ?>
<?php while ( have_posts() ) : the_post(); ?>

	<?php if ( get_post_type() == 'page' ) : ?>
	<?php /* How to display pages. */ ?>

			<div id="page-<?php the_ID(); ?>" <?php post_class(); ?>>
				<h2 class="entry-title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
				
		<?php if (!bm_is_iphone()) { ?>
				<div class="entry-content">
					<?php the_excerpt(''); ?>
				</div>
				
				<div class="entry-meta">
					<span class="comments-link"><?php comments_popup_link( __( 'Comments (0)', 'basicmaths' ), __( 'Comments (1)', 'basicmaths' ), __( 'Comments (%)', 'basicmaths' ) ) ?></span>
					<?php edit_post_link(__( 'Edit', 'basicmaths' ), '<span class="edit">', '</span>'); ?> 
				</div><!-- .entry-meta -->
		<?php } ?>

			</div><!-- .page -->


	<?php else : ?>
	<?php /* How to display all posts. */ ?>

			<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<h2 class="entry-title"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
				<div class="entry-date">
					<abbr class="date published" title="<?php the_time('Y-m-d') ?>"><?php the_time('j M ’y'); ?></abbr>
					<abbr class="time published" title="<?php the_time('TH:i:sO') ?>"><?php the_time('g:i A'); ?></abbr>
				</div>
				
		<?php if (!bm_is_iphone()) { 
			
			// If the post has an excerpt, show the_excerpt() instead.
			if ( has_excerpt() ) { ?>
				<div class="entry-content">
					<?php the_excerpt(''); ?>
				</div>
			<?php } else { ?>	
				<div class="entry-content">
					<?php the_content(''); ?>
				</div>
			<?php } ?>

				<div class="entry-meta">
				<?php if ( has_excerpt() ) { ?>
					<span class="continue-link"><a href="<?php the_permalink(); ?>"><?php _e( 'Continue Reading&hellip;', 'basicmaths' ) ?></a></span>
				<?php } elseif ( strpos( $post->post_content , "<!--more-->" ) != false ) { ?>
					<span class="continue-link"><a href="<?php the_permalink(); ?>#more-<?php the_ID(); ?>"><?php _e( 'Continue Reading&hellip;', 'basicmaths' ) ?></a></span>
				<?php } ?>
					<span class="comments-link"><?php comments_popup_link( __( 'Reacties (0)', 'basicmaths' ), __( 'Reacties (1)', 'basicmaths' ), __( 'Reacties (%)', 'basicmaths' ) ) ?></span>
					<?php edit_post_link(__( 'Edit', 'basicmaths' ), '<span class="edit">', '</span>'); ?> 
				</div><!-- .entry-meta -->
		<?php } ?>

			</div><!-- .post -->
	
	<?php endif; ?>

<?php comments_template('', true); ?>

<?php endwhile; // End the loop. Whew. ?>
	
<?php if (  $wp_query->max_num_pages > 1 ) : ?>
			<div class="nextprev pagination">
				<div class="nav-previous"><?php next_posts_link(__( '<span class="nextprev-arrow">&lsaquo;</span> <span class="nextprev-link-title">Older posts</span>', 'basicmaths' )) ?></div>
				<div class="nav-next"><?php previous_posts_link(__( '<span class="nextprev-arrow">&rsaquo;</span> <span class="nextprev-link-title">Newer posts</span>', 'basicmaths' )) ?></div>
			</div><!-- .nextprev -->
<?php endif; ?>
