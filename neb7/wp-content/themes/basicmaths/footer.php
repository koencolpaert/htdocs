<?php
global $options;
foreach ($options as $value) {
    if (get_option( $value['id'] ) === FALSE) { $$value['id'] = $value['std']; }
    else { $$value['id'] = get_option( $value['id'] ); }
    }
?>
		<div id="footer">
			<p><?php echo stripslashes($bm_footer_text); ?> <a href="<?php bloginfo('rss2_url'); ?>" rel="rss feed" class="rss-feed"><?php _e('RSS feed', 'basicmaths') ?></a>. <?php _e('Theme designed by <a href="http://www.koencolpaert.be" rel="designer" class="designer">Koen Colpaert</a> version 7.0', 'basicmaths') ?></p>
		</div>

</div><!-- #wrapper -->

<?php wp_footer(); ?>

</body>
</html>